Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/users/activate', to: 'users#active'
  resources :users

  resource :session

  resources :bands do
    resources :albums, only: [:new]
  end

  resources :albums do
    resources :tracks, only: [:new]
  end

  resources :tracks, only: [:show, :edit, :create, :update, :destroy] do
    resources :notes, only: [:new, :create, :destroy]
  end

end
