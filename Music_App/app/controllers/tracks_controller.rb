class TracksController < ApplicationController
  before_action :ensure_logged_in

  def show
    @track = Track.find(params[:id])
    @notes = @track.notes
  end

  def new
    @track = Track.new
    @albums = Album.all
  end

  def create
    @track = Track.new(track_params)
    if @track.save
      redirect_to track_url(@track)
    else
      flash.now[:errors] = @track.errors.full_messages
      render :new
    end
  end

  def edit
    @track = Track.find(params[:id])
    @albums = Album.all
  end

  def update
    @track = Track.find(params[:id])
    if @track.update(track_params)
      redirect_to track_url(@track)
    else
      flash.now[:errors] = @track.errors.full_messages
      render :edit
    end
  end

  def destroy
    @track = Track.find(params[:id])
    @track.destroy
    redirect_to albums_url
  end

  private

  def track_params
    params.require(:track).permit(:title, :ord, :bonus, :album_id, :lyrics)
  end

end
