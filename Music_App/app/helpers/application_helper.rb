module ApplicationHelper
  include ERB::Util

  def ugly_lyrics(lyrics)
    html = "<pre>"
    lyrics.split(" ").each_slice(6) do |words|
      html += "&#9835 #{words.join(" ")} <br>"
    end
    html += "</pre>"
    html.html_safe
  end

end
